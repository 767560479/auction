import { Component, OnInit } from '@angular/core';
import {Product, ProductService} from '../shared/product.service';
import {FormControl} from '@angular/forms';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  private products: Product[];

  // 保存搜索关键字
  private keyword: string;

  private titleFilter: FormControl = new FormControl();

  private imgUrl = '';

  constructor(private productService: ProductService) {
    this.titleFilter.valueChanges.debounceTime(500)
      .subscribe(
        value => this.keyword = value
      );
  }

  ngOnInit() {
     this.products = this.productService.getProducts();
  }
}


