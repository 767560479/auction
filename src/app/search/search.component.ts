import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProductService} from '../shared/product.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  forModel: FormGroup;

  categorices: string[];

  constructor(private productServcie: ProductService) {
    let  fb = new FormBuilder();
    this.forModel = fb.group({
      title: ['', Validators.minLength(3)],
      price: [null, this.positiveNumberValidator],
      category: ['-1']
    });
  }

  ngOnInit() {
    this.categorices = this.productServcie.getAllCategories();
  }

  positiveNumberValidator(control: FormControl): any {
    if (!control.value) {
      return null;
    }
    let price = parseInt(control.value);

    if ( price > 0) {
      return null;
    } else {
      return {positiveNumberValidator : true};
    }
  }

  onSearch() {
    if (this.forModel.valid) {
      console.log(this.forModel.value);
    }
  }

}
