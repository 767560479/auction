import { Injectable } from '@angular/core';

@Injectable()
export class ProductService {

   private products: Product[] = [
    new Product(1, '第一个商品 ', 1.99 , 3.5 , '这是第一个商品，是我在学习慕课网angular入门学习创建的', [  '电子产品', '电子设备' ]),
    new Product(2, '第二个商品 ', 1.99 , 2.5 , '这是第二个商品，是我在学习慕课网angular入门学习创建的', [  '电子产品', '电子设备' ]),
    new Product(3, '第三个商品 ', 1.99 , 3.5 , '这是第三个商品，是我在学习慕课网angular入门学习创建的', [  '电子产品', '硬件设备' ]),
    new Product(4, '第四个商品 ', 1.99 , 1.5 , '这是第四个商品，是我在学习慕课网angular入门学习创建的', [  '电子产品', '电子设备' ]),
    new Product(5 , '第五个商品 ', 1.99 , 3.5 , '这是第五个商品，是我在学习慕课网angular入门学习创建的', [  '电子产品', '电子设备' ])
  ];

   private comments: Comment[] = [
     new Comment(1, 1, '2017-02-02 22:22:23', '张三', 3, '东西不错1'),
     new Comment(2, 1, '2017-03-02 22:12:23', '李四', 4, '东西不错2'),
     new Comment(3, 1, '2017-07-02 12:22:23', '张三', 2, '东西不错3'),
     new Comment(4, 2, '2017-02-02 22:22:23', '张三', 3, '东西不错6'),
   ];

  constructor() { }

  getAllCategories(): string[] {
    return ['电子产品', '硬件设备', '图书'];
  }

    getProducts(): Product[] {
      return this.products;
    }
    getProduct(id: number): Product {
      // tslint:disable-next-line:triple-equals
      return this.products.find((product) => product.id == id);
    }

    getCommentsForProductId(id: number): Comment[] {
     // filter 过滤器
      // tslint:disable-next-line:triple-equals
        return this.comments.filter((comment: Comment) => comment.productId == id);
    }
}


export class Product {
  constructor(
    public id: number,
    public title: string,
    public price: number,
    public rating: number,
    public desc: string,
    public categories: Array<string>
  ) {  }
}

export class Comment {
  constructor(public id: number,
              public productId: number,
              public timestamp: string,
              public user: string,
              public rating: number,
              public content: string
              ) {}
}
